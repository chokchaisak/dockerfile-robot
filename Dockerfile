FROM python:3.7-alpine3.12

LABEL name="Docker build demo Robot Framework"
MAINTAINER "TestDD"

ENV SCREEN_WIDTH 1920
ENV SCREEN_HEIGHT 1080
ENV SCREEN_DEPTH 16

COPY requirements.txt /tmp/requirements.txt

RUN apk update ;\
    apk add --no-cache chromium chromium-chromedriver udev xvfb firefox-esr ttf-freefont;\
    pip install --upgrade pip ;\
    pip install --no-cache-dir -r /tmp/requirements.txt ;\
    sed -i "s/self._arguments\ =\ \[\]/self._arguments\ =\ \['\--headless'\,'\--no-sandbox'\,\'--disable-gpu'\,'\--disable-dev-shm-usage'\]/" $(python -c "import site; print(site.getsitepackages()[0])")/selenium/webdriver/chrome/options.py ;\
    apk info -vv | sort ;\
    pip freeze ;\
    rm -rf /var/cache/apk/* /tmp/requirements.txt
