# dockerfile-robot



## Getting started

docker build -f 'Dockerfile' -t [tagname]:[version] .

docker run --rm -v /opt/jenkins-app/jenkins_home/workspace/[jobJenkinsName]:[pathfile] [tagname]:[version] sh -c "robot -d /[pathfile]/result [pathfile]"
